global.player;
global.colors = [];
global.dataTrack = null;
var elems = [];
const LOCALSTORAGE_ACCESS_TOKEN_KEY = 'spotify-audio-analysis-playback-token';
const LOCALSTORAGE_ACCESS_TOKEN_EXPIRY_KEY = 'spotify-audio-analysis-playback-token-expires-in';
const accessToken = localStorage.getItem(LOCALSTORAGE_ACCESS_TOKEN_KEY);
var isConnected = false;

if (localStorage.getItem(LOCALSTORAGE_ACCESS_TOKEN_KEY) &&
    parseInt(parseInt(localStorage.getItem(LOCALSTORAGE_ACCESS_TOKEN_EXPIRY_KEY))) > Date.now()) {
    isConnected = true;
} else {
    if (window.location.hash) {
        const hash = parseHash(window.location.hash);
        if (hash['access_token'] &&
            hash['expires_in']) {
            localStorage.setItem(LOCALSTORAGE_ACCESS_TOKEN_KEY, hash['access_token']);
            localStorage.setItem(LOCALSTORAGE_ACCESS_TOKEN_EXPIRY_KEY, Date.now() + 990 * parseInt(hash['expires_in']));
            window.location = "/";
            isConnected = true;
        }
    }
}

let deviceId = '';



var img = new Image;


global.drawAnalysis = function (data) {
    var sectionsTrack =data.sections
    startSketch()
    setTimeout(function(){
        const featuresChart = document.getElementById('features-chart');
        featuresChart.style.width = document.body.offsetWidth;
        featuresChart.width = document.body.offsetWidth;

        const width = featuresChart.width;
        const ctx = featuresChart.getContext("2d");

        //Créer les sections
        sectionsTrack.forEach((section, sectionIndex) => {
            ctx.fillStyle = colors[sectionIndex % colors.length];
            ctx.fillRect(section.start / data.track.duration * width, 0, section.duration / data.track.duration * width, featuresChart.height);
        });

        //Barre de défilement de la musique
        function provideAnimationFrame(timestamp) {
            player && player.getCurrentState().then(state => {
                ctx.clearRect(0, 0, featuresChart.width, featuresChart.height);
                ctx.drawImage(img, 0, 0);
                ctx.fillStyle = "#000";
                const position = state.position / 1000 / data.track.duration * width
                ctx.fillRect(position - 2, 0, 5, featuresChart.height);
                window.requestAnimationFrame(provideAnimationFrame);
            }).catch(e => {
                console.error("Animation: ", e);
                window.requestAnimationFrame(provideAnimationFrame);
            });
        }

        window.requestAnimationFrame(provideAnimationFrame);
        img.src = featuresChart.toDataURL('png');
    },100)
}


//Avoir l'analyse du son
global.getAnalysis = function (id) {
    section = document.getElementById("section1");
    section.firstChild.remove();
    let query = '/analysis?id=' + id;
    document.getElementById('imgButton').setAttribute('src', '/img/pause-symbol.png');
    isPlaying = true;
    document.getElementById("analyse").style.filter = "none";
    document.getElementById("popup").style.display = "none";
    document.getElementById("bg_logo").classList.add("fadeout");
    document.getElementById("playButton").classList.add("fadein");
    document.getElementById("bg_blur").style.display = "none";
    return fetch(query).then(e => e.json()).then(data => {
        dataTrack = data;
        drawAnalysis(data);
        fetch(`https://api.spotify.com/v1/me/player/play${deviceId && `?device_id=${deviceId}`}`, {
            method: "PUT",
            body: JSON.stringify({"uris": [`spotify:track:${id}`]}),
            headers: {
                'Authorization': `Bearer ${accessToken}`
            }
        }).catch(e => console.error(e));
    });
};


//Lire la chanson choisi
var isPlaying = true;
global.onSpotifyPlayerAPIReady = function () {
    player = new global.Spotify.Player({
        name: 'Audio Analysis Player',
        getOauthToken: function (callback) {
            callback(accessToken);
        },
        volume: 0.8
    });

    // Ready
    player.on('ready', function (data) {
        deviceId = data.device_id;
        setTimeout(() => {
            fetch('https://api.spotify.com/v1/me/player', {
                method: "PUT",
                body: JSON.stringify({
                    device_ids: [
                        data.device_id
                    ],
                    play: false
                }),
                headers: {
                    'Authorization': `Bearer ${accessToken}`
                }
            }).catch(e => console.error(e));
        }, 100);
    });
    // Connect to the player!
    player.connect();

    //Play ou pause the player
    const playButton = document.getElementById('imgButton');

    playButton.addEventListener('click', function (event) {
        player.togglePlay();
        if (isPlaying) {
            playButton.setAttribute('src', '/img/play-button.png');
            isPlaying = false;
            document.getElementById("analyse").style.filter = "blur(3px)";
            document.getElementById("popup").style.display = "block";
            document.getElementById("bg_logo").classList.add("fadeout");
            document.getElementById("bg_blur").style.display = "block";
        } else {
            playButton.setAttribute('src', '/img/pause-symbol.png')
            isPlaying = true;
            document.getElementById("analyse").style.filter = "none";
            document.getElementById("popup").style.display = "none";
            document.getElementById("bg_logo").classList.add("fadeout");
            document.getElementById("bg_blur").style.display = "none";
        }
    });
}

function parseHash(hash) {
    return hash
        .substring(1)
        .split('&')
        .reduce(function (initial, item) {
            if (item) {
                var parts = item.split('=');
                initial[parts[0]] = decodeURIComponent(parts[1]);
            }
            return initial;
        }, {});
}


document.addEventListener('DOMContentLoaded', () => {

    //Utilisateur Auth
    if (isConnected) {
        document.getElementById('isConnected').innerHTML = '<input id="search" type="text" maxlength="100" placeholder="Choisir une musique"><img src="/img/loop-spotify.png" alt="fdvf" />';

        //Chercher une musique
        const input = document.querySelector('input');
        input.addEventListener('input', function (event) {
            if (input.value.length >= 3) {
                const searchQuery = '/search?query=' + (query => !query ? "cut to the feeling" : query)(input.value);
                fetch(searchQuery).then(e => e.json()).then(data => {
                    document.getElementById('results').innerHTML = data.tracks.items
                        .map(track => `<li class="text-salmon" onClick="getAnalysis(&apos;${track.id}&apos;)"><span class="trackName">${track.name}</span><span class="trackArtist">${track.artists[0].name}</span><span class="trackImg"><img class="imgAlbulm" src="${track.album.images[2].url}"></span></li>`)
                        .join('\n');
                }).catch(error => {
                    document.getElementById('results').innerHTML = error;
                });
            }
        });

    } else {

        //Utilisateur Non- Auth
        document.getElementById('isConnected').innerHTML = '<a id="login" href="" class="btn btn-sm btn-primary">Log In With Spotify</a>';
        document.getElementById('login').addEventListener('click', function (e) {
            e.preventDefault();
            fetch('/spotifyRedirectUri')
                .then(e => e.json())
                .then(data => {
                    window.location = data.redirectUri;
                })
                .catch(error => {
                    alert("Failed to prepare for Spotify Authentication")
                });
        });
    }


});
