// Import old analysis.js
require('./analysis');

///////////////////
/*
import p5 from 'p5';

/!**
 * DEFINE ALL GLABAL VARIABLES HERE
 *!/
let width = window.innerWidth;
let height = window.innerHeight - 100;

let myCanvas;
let c1;
let c2;
let c3;
let c4;
let radiusCoef = 0;
let maxScale = (height / 50);
let sections = [];
let trackProgress = 0;

const sketch = (p5) => {
    p5.preload = () => {
    };

    p5.setup = () => {
        c1 = p5.color("#6876AC");
        c2 = p5.color("#D8315B");
        c3 = p5.color("#D8315B");
        c4 = p5.color("#E57995");
        myCanvas = p5.createCanvas(width, height);
        myCanvas.parent('section1');
        p5.noStroke();
    };

    p5.draw = () => {
        // THE DRAAAAWWWWWW

        player.getCurrentState().then(state => {
            if (dataTrack != null && state.paused == false) {
                trackProgress = (state.position / state.duration) * 100;
                if (state.position + 1000 >= (dataTrack.sections[0].start * 1000)) {
                    dataTrack.sections[0].initProgress = (trackProgress);
                    sections.push(dataTrack.sections[0]);

                    dataTrack.sections.shift();
                }
            }
        });
        p5.background(255);
        sections.forEach((section, sectionIndex) => {
            var scaleProgress = maxScale / (100 / (trackProgress - section.initProgress));
            p5.push();
            p5.translate(width * 0.5, height * 0.5);
            if (sectionIndex % 2 == 0) {
                p5.rotate(scaleProgress * (sectionIndex + 1));
                p5.fill(c1)
            } else {
                p5.rotate(-scaleProgress * (sectionIndex + 1));
                p5.fill(c2)
            }

            p5.scale(scaleProgress);
            star(0, 0, 25, 50, p5.round(section.duration));

            p5.pop();
        });
    };
};

let canvas = new p5(sketch);

function star(x, y, radius1, radius2, npoints) {
    var angle = p5.TWO_PI / npoints;
    var halfAngle = angle / 2.0;
    p5.beginShape();
    for (var a = 0; a < p5.TWO_PI; a += angle) {
        var sx = x + p5.cos(a) * radius2;
        var sy = y + p5.sin(a) * radius2;
        vertex(sx, sy);
        sx = x + p5.cos(a + halfAngle) * radius1;
        sy = y + p5.sin(a + halfAngle) * radius1;
        vertex(sx, sy);
    }
    p5.endShape(p5.CLOSE);
}*/
