let width = window.innerWidth;
let height = window.innerHeight + 80;
let maxScale = (height / 50) / 1.5;

let traces, particules, etoiles;
let sections = [];
let beats = [];

let trackProgress = 0;
let radiusCoef = -60;
let coefAcuumulation = 0;
let accumulation = 200;

let colorState = 0;
let image;
let imageLink = 'https://i.scdn.co/image/9e5288926fadb82f873ccf2b45300c3a6f65fa14';
let playing = false;



function startSketch() {
    let sketch = function (p) {
        p.preload = function () {
            generateColors();
        };

        p.setup = function () {     
            if (particules && traces && etoiles) {
                p.clear;
                sections = [];
                beats = [];
                particules.remove();
                traces.remove();
                etoiles.remove();
            };   
            let myCanvas = p.createCanvas(width, height);
            particules = p.createGraphics(width, height);
            traces = p.createGraphics(width, height);
            etoiles = p.createGraphics(width, height);
            myCanvas.parent('section1');
        };

        p.draw = function () {
            player.getCurrentState().then(state => {

                //Player state & couleurs
                if (state !== null) {
                    playing = true;
                }
                if (!playing && colorState === 1) {
                    return;
                }
                if (colorState === 0) {
                    generateColors();
                }
                if (colorState < 2) {
                    return;
                }

                //Player Progress
                trackProgress = (state.position / state.duration) * 100;
                if (dataTrack != null && state.paused == false) {

                    //Animation Progress
                    p.background(0)
                    accumulation = accumulation + 0.4;
                    radiusCoef = radiusCoef - 0.7
                    if (beats.length % 2 == 0) {
                        coefAcuumulation = coefAcuumulation + 0.0011;
                    } else {
                        coefAcuumulation = coefAcuumulation - 0.001;
                    }

                    //Sections Progress
                    if (dataTrack.sections.length >0){
                        if (state.position-1 >= (dataTrack.sections[0].start * 1000)) {
                            dataTrack.sections[0].initProgress = (trackProgress);
                            sections.push(dataTrack.sections[0]);
                            
                            dataTrack.sections.shift();
                        }
    
                    }
                    
                    //Segments Progress
                    if (state.position >= (dataTrack.segments[0].start * 1000)) {
                        if (dataTrack.segments[0].confidence > 0.7) {//Le 10 pourrais étre une variable liée a quelque chose, cela donne plus ou moin de vitesse au rythme
                            dataTrack.segments[0].scaleProgress = Math.floor(Math.random() * 5) + 2;;
                            dataTrack.segments[0].accumulation = Math.floor(Math.random() * 1000) + 400
                            dataTrack.segments[0].color = Math.floor(Math.random() * 4);
                            beats.push(dataTrack.segments[0]);
                        }
                        if (p.round(dataTrack.segments[0].loudness_max) > (radiusCoef - 7)) {//Le 10 pourrais étre une variable liée a quelque chose, cela donne plus ou moin de vitesse au rythme
                            radiusCoef = p.round(dataTrack.segments[0].loudness_max);
                        }
                        dataTrack.segments.shift();
                    }
                }



                //Particules
                particules.clear();
                beats.forEach((beat, beatIndex) => {
                    particules.push();
                    particules.translate(width * 0.5, height * 0.5);
                    particules.rotate(beat.scaleProgress * (beatIndex + 1));
                    particules.fill(colors[beat.color])
                    particules.scale((1 / beat.scaleProgress) + coefAcuumulation);
                    //particules.noStroke();
                    star_two(beat.accumulation, beat.accumulation, 50, 50, 3);
                    particules.pop();
                });



                //Etoiles & Traces
                etoiles.clear();
                sections.forEach((section, sectionIndex) => {
                    var scaleProgress = maxScale / (100 / (trackProgress - section.initProgress));
                    var radiusCrazy = (60 + (radiusCoef / ((trackProgress - section.initProgress) / 10)))
                    if (radiusCrazy < 0) {
                        traceBackground(section,sectionIndex,radiusCrazy,scaleProgress);
                    }
                    etoiles.push();
                    etoiles.translate(width * 0.5, height * 0.5);
                    if (sectionIndex % 2 === 0) {
                        etoiles.rotate(scaleProgress);
                    } else {
                        etoiles.rotate(-scaleProgress);
                    }
                    etoiles.fill(colors[((sections.length + sectionIndex) % 4)])
                    etoiles.scale((scaleProgress + 1) / 2);
                    //etoiles.noStroke();
                    star(0, 0, 25, radiusCrazy, etoiles.round(section.duration));
                    etoiles.pop();
                });


                //Grapiques
                p.image(traces, 0, 0);
                p.image(particules, 0, 0);
                p.image(etoiles, 0, 0);
            });
        };



        let traceBackground = function (section,sectionIndex,radiusCrazy,scaleProgress) {
            traces.push();
            traces.translate(width * 0.5, height * 0.5);
            if (sectionIndex % 2 === 0) {
                traces.rotate(scaleProgress);
            } else {
                traces.rotate(-scaleProgress);
            }
            traces.fill(colors[(sectionIndex % 4)])
            traces.scale((scaleProgress + 1) / 2);
            star_one(0, 0, 25, radiusCrazy, traces.round(section.duration));
            traces.pop();
        }


        let star = function (x, y, radius1, radius2, npoints) {
            let angle = etoiles.TWO_PI / npoints;
            let halfAngle = angle / 2.0;
            etoiles.beginShape();
            for (let a = 0; a < etoiles.TWO_PI; a += angle) {
                let sx = x + etoiles.cos(a) * radius2;
                let sy = y + etoiles.sin(a) * radius2;
                etoiles.vertex(sx, sy);
                sx = x + etoiles.cos(a + halfAngle) * radius1;
                sy = y + etoiles.sin(a + halfAngle) * radius1;
                etoiles.vertex(sx, sy);
            }
            etoiles.endShape(etoiles.CLOSE);
            etoiles.noStroke();
        };
        let star_one = function (x, y, radius1, radius2, npoints) {
            let angle = traces.TWO_PI / npoints;
            let halfAngle = angle / 2.0;
            traces.beginShape();
            for (let a = 0; a < traces.TWO_PI; a += angle) {
                let sx = x + traces.cos(a) * radius2;
                let sy = y + traces.sin(a) * radius2;
                traces.vertex(sx, sy);
                sx = x + traces.cos(a + halfAngle) * radius1;
                sy = y + traces.sin(a + halfAngle) * radius1;
                traces.vertex(sx, sy);
            }
            traces.endShape(traces.CLOSE);
            traces.noStroke();
        };
        let star_two = function (x, y, radius1, radius2, npoints) {
            let angle = particules.TWO_PI / npoints;
            let halfAngle = angle / 2.0;
            particules.beginShape();
            for (let a = 0; a < particules.TWO_PI; a += angle) {
                let sx = x + particules.cos(a) * radius2;
                let sy = y + particules.sin(a) * radius2;
                particules.vertex(sx, sy);
                sx = x + particules.cos(a + halfAngle) * radius1;
                sy = y + particules.sin(a + halfAngle) * radius1;
                particules.vertex(sx, sy);
            }
            particules.endShape(particules.CLOSE);
            particules.noStroke();
        };


        let generateColors = function () {
            if (colorState === 2) { return; }
            player.getCurrentState().then(state => {
                colorState = 1;
                if (!state) {
                    console.error('User is not playing music through the Web Playback SDK');
                    return;
                }

                let { current_track, next_tracks: [next_track] } = state.track_window;

                imageLink = current_track.album.images[1].url;
                image = p.loadImage(imageLink);

                let v = new Vibrant(imageLink);
                v.getPalette((err, palette) => {
                    // Old Method, Enable if new not working
                    //colors[0] = p.color(palette.Vibrant.getRgb());
                    //colors[1] = p.color(palette.DarkVibrant.getRgb());
                    //colors[2] = p.color(palette.LightVibrant.getRgb());
                    //colors[3] = p.color(palette.DarkMuted.getRgb());

                    let vibrant = p.color(palette.Vibrant.getRgb());
                    let mute    = p.color(palette.DarkMuted.getRgb());

                    colors[0]   = vibrant;
                    colors[1]   = p.lerpColor(vibrant, mute, 0.33);
                    colors[2]   = p.lerpColor(vibrant, mute, 0.66);
                    colors[3]   = mute;

                    colorState = 2;
                });
            });
        };
    };

    let myp5 = new p5(sketch);
}


var elem = document.documentElement;
var isFullScreen = false;

document.addEventListener('keypress', (event) => {
    if (event.key === 'f' && !isFullScreen) {
        openFullscreen();
    } else if (event.key === 'f' && isFullScreen) {
        closeFullscreen();
    }
})

function openFullscreen() {
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
        open();
    } else if (elem.mozRequestFullScreen) { /* Firefox */
        elem.mozRequestFullScreen();
        open();
    } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
        elem.webkitRequestFullscreen();
        open();
    } else if (elem.msRequestFullscreen) { /* IE/Edge */
        elem.msRequestFullscreen();
        open();
    }
    isFullScreen = true;
}

function closeFullscreen() {
    if (document.exitFullscreen) {
        document.exitFullscreen();
        close();
    } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
        close();
    } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
        close();
    } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
        close();
    }
    isFullScreen = false;
}

function open() {
    document.getElementById("close").style.display = "block";
    document.getElementById("open").style.display = "none";
}

function close() {
    document.getElementById("close").style.display = "none";
    document.getElementById("open").style.display = "block";
}

