(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app"],{

/***/ "./assets/js/analysis.js":
/*!*******************************!*\
  !*** ./assets/js/analysis.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {global.player;
global.colors = [];
global.dataTrack = null;
var elems = [];
var LOCALSTORAGE_ACCESS_TOKEN_KEY = 'spotify-audio-analysis-playback-token';
var LOCALSTORAGE_ACCESS_TOKEN_EXPIRY_KEY = 'spotify-audio-analysis-playback-token-expires-in';
var accessToken = localStorage.getItem(LOCALSTORAGE_ACCESS_TOKEN_KEY);
var isConnected = false;

if (localStorage.getItem(LOCALSTORAGE_ACCESS_TOKEN_KEY) && parseInt(parseInt(localStorage.getItem(LOCALSTORAGE_ACCESS_TOKEN_EXPIRY_KEY))) > Date.now()) {
  isConnected = true;
} else {
  if (window.location.hash) {
    var hash = parseHash(window.location.hash);

    if (hash['access_token'] && hash['expires_in']) {
      localStorage.setItem(LOCALSTORAGE_ACCESS_TOKEN_KEY, hash['access_token']);
      localStorage.setItem(LOCALSTORAGE_ACCESS_TOKEN_EXPIRY_KEY, Date.now() + 990 * parseInt(hash['expires_in']));
      window.location = "/";
      isConnected = true;
    }
  }
}

var deviceId = '';
var img = new Image();

global.drawAnalysis = function (data) {
  var sectionsTrack = data.sections;
  startSketch();
  setTimeout(function () {
    var featuresChart = document.getElementById('features-chart');
    featuresChart.style.width = document.body.offsetWidth;
    featuresChart.width = document.body.offsetWidth;
    var width = featuresChart.width;
    var ctx = featuresChart.getContext("2d"); //Créer les sections

    sectionsTrack.forEach(function (section, sectionIndex) {
      ctx.fillStyle = colors[sectionIndex % colors.length];
      ctx.fillRect(section.start / data.track.duration * width, 0, section.duration / data.track.duration * width, featuresChart.height);
    }); //Barre de défilement de la musique

    function provideAnimationFrame(timestamp) {
      player && player.getCurrentState().then(function (state) {
        ctx.clearRect(0, 0, featuresChart.width, featuresChart.height);
        ctx.drawImage(img, 0, 0);
        ctx.fillStyle = "#000";
        var position = state.position / 1000 / data.track.duration * width;
        ctx.fillRect(position - 2, 0, 5, featuresChart.height);
        window.requestAnimationFrame(provideAnimationFrame);
      }).catch(function (e) {
        console.error("Animation: ", e);
        window.requestAnimationFrame(provideAnimationFrame);
      });
    }

    window.requestAnimationFrame(provideAnimationFrame);
    img.src = featuresChart.toDataURL('png');
  }, 100);
}; //Avoir l'analyse du son


global.getAnalysis = function (id) {
  section = document.getElementById("section1");
  section.firstChild.remove();
  var query = '/analysis?id=' + id;
  document.getElementById('imgButton').setAttribute('src', '/img/pause-symbol.png');
  isPlaying = true;
  document.getElementById("analyse").style.filter = "none";
  document.getElementById("popup").style.display = "none";
  document.getElementById("bg_logo").classList.add("fadeout");
  document.getElementById("playButton").classList.add("fadein");
  document.getElementById("bg_blur").style.display = "none";
  return fetch(query).then(function (e) {
    return e.json();
  }).then(function (data) {
    dataTrack = data;
    drawAnalysis(data);
    fetch("https://api.spotify.com/v1/me/player/play".concat(deviceId && "?device_id=".concat(deviceId)), {
      method: "PUT",
      body: JSON.stringify({
        "uris": ["spotify:track:".concat(id)]
      }),
      headers: {
        'Authorization': "Bearer ".concat(accessToken)
      }
    }).catch(function (e) {
      return console.error(e);
    });
  });
}; //Lire la chanson choisi


var isPlaying = true;

global.onSpotifyPlayerAPIReady = function () {
  player = new global.Spotify.Player({
    name: 'Audio Analysis Player',
    getOauthToken: function getOauthToken(callback) {
      callback(accessToken);
    },
    volume: 0.8
  }); // Ready

  player.on('ready', function (data) {
    deviceId = data.device_id;
    setTimeout(function () {
      fetch('https://api.spotify.com/v1/me/player', {
        method: "PUT",
        body: JSON.stringify({
          device_ids: [data.device_id],
          play: false
        }),
        headers: {
          'Authorization': "Bearer ".concat(accessToken)
        }
      }).catch(function (e) {
        return console.error(e);
      });
    }, 100);
  }); // Connect to the player!

  player.connect(); //Play ou pause the player

  var playButton = document.getElementById('imgButton');
  playButton.addEventListener('click', function (event) {
    player.togglePlay();

    if (isPlaying) {
      playButton.setAttribute('src', '/img/play-button.png');
      isPlaying = false;
      document.getElementById("analyse").style.filter = "blur(3px)";
      document.getElementById("popup").style.display = "block";
      document.getElementById("bg_logo").classList.add("fadeout");
      document.getElementById("bg_blur").style.display = "block";
    } else {
      playButton.setAttribute('src', '/img/pause-symbol.png');
      isPlaying = true;
      document.getElementById("analyse").style.filter = "none";
      document.getElementById("popup").style.display = "none";
      document.getElementById("bg_logo").classList.add("fadeout");
      document.getElementById("bg_blur").style.display = "none";
    }
  });
};

function parseHash(hash) {
  return hash.substring(1).split('&').reduce(function (initial, item) {
    if (item) {
      var parts = item.split('=');
      initial[parts[0]] = decodeURIComponent(parts[1]);
    }

    return initial;
  }, {});
}

document.addEventListener('DOMContentLoaded', function () {
  //Utilisateur Auth
  if (isConnected) {
    document.getElementById('isConnected').innerHTML = '<input id="search" type="text" maxlength="100" placeholder="Choisir une musique"><img src="/img/loop-spotify.png" alt="fdvf" />'; //Chercher une musique

    var input = document.querySelector('input');
    input.addEventListener('input', function (event) {
      if (input.value.length >= 3) {
        var searchQuery = '/search?query=' + function (query) {
          return !query ? "cut to the feeling" : query;
        }(input.value);

        fetch(searchQuery).then(function (e) {
          return e.json();
        }).then(function (data) {
          document.getElementById('results').innerHTML = data.tracks.items.map(function (track) {
            return "<li class=\"text-salmon\" onClick=\"getAnalysis(&apos;".concat(track.id, "&apos;)\"><span class=\"trackName\">").concat(track.name, "</span><span class=\"trackArtist\">").concat(track.artists[0].name, "</span><span class=\"trackImg\"><img class=\"imgAlbulm\" src=\"").concat(track.album.images[2].url, "\"></span></li>");
          }).join('\n');
        }).catch(function (error) {
          document.getElementById('results').innerHTML = error;
        });
      }
    });
  } else {
    //Utilisateur Non- Auth
    document.getElementById('isConnected').innerHTML = '<a id="login" href="" class="btn btn-sm btn-primary">Log In With Spotify</a>';
    document.getElementById('login').addEventListener('click', function (e) {
      e.preventDefault();
      fetch('/spotifyRedirectUri').then(function (e) {
        return e.json();
      }).then(function (data) {
        window.location = data.redirectUri;
      }).catch(function (error) {
        alert("Failed to prepare for Spotify Authentication");
      });
    });
  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./assets/js/app.js":
/*!**************************!*\
  !*** ./assets/js/app.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Import old analysis.js
__webpack_require__(/*! ./analysis */ "./assets/js/analysis.js"); ///////////////////

/*
import p5 from 'p5';

/!**
 * DEFINE ALL GLABAL VARIABLES HERE
 *!/
let width = window.innerWidth;
let height = window.innerHeight - 100;

let myCanvas;
let c1;
let c2;
let c3;
let c4;
let radiusCoef = 0;
let maxScale = (height / 50);
let sections = [];
let trackProgress = 0;

const sketch = (p5) => {
    p5.preload = () => {
    };

    p5.setup = () => {
        c1 = p5.color("#6876AC");
        c2 = p5.color("#D8315B");
        c3 = p5.color("#D8315B");
        c4 = p5.color("#E57995");
        myCanvas = p5.createCanvas(width, height);
        myCanvas.parent('section1');
        p5.noStroke();
    };

    p5.draw = () => {
        // THE DRAAAAWWWWWW

        player.getCurrentState().then(state => {
            if (dataTrack != null && state.paused == false) {
                trackProgress = (state.position / state.duration) * 100;
                if (state.position + 1000 >= (dataTrack.sections[0].start * 1000)) {
                    dataTrack.sections[0].initProgress = (trackProgress);
                    sections.push(dataTrack.sections[0]);

                    dataTrack.sections.shift();
                }
            }
        });
        p5.background(255);
        sections.forEach((section, sectionIndex) => {
            var scaleProgress = maxScale / (100 / (trackProgress - section.initProgress));
            p5.push();
            p5.translate(width * 0.5, height * 0.5);
            if (sectionIndex % 2 == 0) {
                p5.rotate(scaleProgress * (sectionIndex + 1));
                p5.fill(c1)
            } else {
                p5.rotate(-scaleProgress * (sectionIndex + 1));
                p5.fill(c2)
            }

            p5.scale(scaleProgress);
            star(0, 0, 25, 50, p5.round(section.duration));

            p5.pop();
        });
    };
};

let canvas = new p5(sketch);

function star(x, y, radius1, radius2, npoints) {
    var angle = p5.TWO_PI / npoints;
    var halfAngle = angle / 2.0;
    p5.beginShape();
    for (var a = 0; a < p5.TWO_PI; a += angle) {
        var sx = x + p5.cos(a) * radius2;
        var sy = y + p5.sin(a) * radius2;
        vertex(sx, sy);
        sx = x + p5.cos(a + halfAngle) * radius1;
        sy = y + p5.sin(a + halfAngle) * radius1;
        vertex(sx, sy);
    }
    p5.endShape(p5.CLOSE);
}*/

/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ })

},[["./assets/js/app.js","runtime"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvYW5hbHlzaXMuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2FwcC5qcyIsIndlYnBhY2s6Ly8vKHdlYnBhY2spL2J1aWxkaW4vZ2xvYmFsLmpzIl0sIm5hbWVzIjpbImdsb2JhbCIsInBsYXllciIsImNvbG9ycyIsImRhdGFUcmFjayIsImVsZW1zIiwiTE9DQUxTVE9SQUdFX0FDQ0VTU19UT0tFTl9LRVkiLCJMT0NBTFNUT1JBR0VfQUNDRVNTX1RPS0VOX0VYUElSWV9LRVkiLCJhY2Nlc3NUb2tlbiIsImxvY2FsU3RvcmFnZSIsImdldEl0ZW0iLCJpc0Nvbm5lY3RlZCIsInBhcnNlSW50IiwiRGF0ZSIsIm5vdyIsIndpbmRvdyIsImxvY2F0aW9uIiwiaGFzaCIsInBhcnNlSGFzaCIsInNldEl0ZW0iLCJkZXZpY2VJZCIsImltZyIsIkltYWdlIiwiZHJhd0FuYWx5c2lzIiwiZGF0YSIsInNlY3Rpb25zVHJhY2siLCJzZWN0aW9ucyIsInN0YXJ0U2tldGNoIiwic2V0VGltZW91dCIsImZlYXR1cmVzQ2hhcnQiLCJkb2N1bWVudCIsImdldEVsZW1lbnRCeUlkIiwic3R5bGUiLCJ3aWR0aCIsImJvZHkiLCJvZmZzZXRXaWR0aCIsImN0eCIsImdldENvbnRleHQiLCJmb3JFYWNoIiwic2VjdGlvbiIsInNlY3Rpb25JbmRleCIsImZpbGxTdHlsZSIsImxlbmd0aCIsImZpbGxSZWN0Iiwic3RhcnQiLCJ0cmFjayIsImR1cmF0aW9uIiwiaGVpZ2h0IiwicHJvdmlkZUFuaW1hdGlvbkZyYW1lIiwidGltZXN0YW1wIiwiZ2V0Q3VycmVudFN0YXRlIiwidGhlbiIsInN0YXRlIiwiY2xlYXJSZWN0IiwiZHJhd0ltYWdlIiwicG9zaXRpb24iLCJyZXF1ZXN0QW5pbWF0aW9uRnJhbWUiLCJjYXRjaCIsImUiLCJjb25zb2xlIiwiZXJyb3IiLCJzcmMiLCJ0b0RhdGFVUkwiLCJnZXRBbmFseXNpcyIsImlkIiwiZmlyc3RDaGlsZCIsInJlbW92ZSIsInF1ZXJ5Iiwic2V0QXR0cmlidXRlIiwiaXNQbGF5aW5nIiwiZmlsdGVyIiwiZGlzcGxheSIsImNsYXNzTGlzdCIsImFkZCIsImZldGNoIiwianNvbiIsIm1ldGhvZCIsIkpTT04iLCJzdHJpbmdpZnkiLCJoZWFkZXJzIiwib25TcG90aWZ5UGxheWVyQVBJUmVhZHkiLCJTcG90aWZ5IiwiUGxheWVyIiwibmFtZSIsImdldE9hdXRoVG9rZW4iLCJjYWxsYmFjayIsInZvbHVtZSIsIm9uIiwiZGV2aWNlX2lkIiwiZGV2aWNlX2lkcyIsInBsYXkiLCJjb25uZWN0IiwicGxheUJ1dHRvbiIsImFkZEV2ZW50TGlzdGVuZXIiLCJldmVudCIsInRvZ2dsZVBsYXkiLCJzdWJzdHJpbmciLCJzcGxpdCIsInJlZHVjZSIsImluaXRpYWwiLCJpdGVtIiwicGFydHMiLCJkZWNvZGVVUklDb21wb25lbnQiLCJpbm5lckhUTUwiLCJpbnB1dCIsInF1ZXJ5U2VsZWN0b3IiLCJ2YWx1ZSIsInNlYXJjaFF1ZXJ5IiwidHJhY2tzIiwiaXRlbXMiLCJtYXAiLCJhcnRpc3RzIiwiYWxidW0iLCJpbWFnZXMiLCJ1cmwiLCJqb2luIiwicHJldmVudERlZmF1bHQiLCJyZWRpcmVjdFVyaSIsImFsZXJ0IiwicmVxdWlyZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUFBLG9EQUFNLENBQUNDLE1BQVA7QUFDQUQsTUFBTSxDQUFDRSxNQUFQLEdBQWdCLEVBQWhCO0FBQ0FGLE1BQU0sQ0FBQ0csU0FBUCxHQUFtQixJQUFuQjtBQUNBLElBQUlDLEtBQUssR0FBRyxFQUFaO0FBQ0EsSUFBTUMsNkJBQTZCLEdBQUcsdUNBQXRDO0FBQ0EsSUFBTUMsb0NBQW9DLEdBQUcsa0RBQTdDO0FBQ0EsSUFBTUMsV0FBVyxHQUFHQyxZQUFZLENBQUNDLE9BQWIsQ0FBcUJKLDZCQUFyQixDQUFwQjtBQUNBLElBQUlLLFdBQVcsR0FBRyxLQUFsQjs7QUFFQSxJQUFJRixZQUFZLENBQUNDLE9BQWIsQ0FBcUJKLDZCQUFyQixLQUNBTSxRQUFRLENBQUNBLFFBQVEsQ0FBQ0gsWUFBWSxDQUFDQyxPQUFiLENBQXFCSCxvQ0FBckIsQ0FBRCxDQUFULENBQVIsR0FBaUZNLElBQUksQ0FBQ0MsR0FBTCxFQURyRixFQUNpRztBQUM3RkgsYUFBVyxHQUFHLElBQWQ7QUFDSCxDQUhELE1BR087QUFDSCxNQUFJSSxNQUFNLENBQUNDLFFBQVAsQ0FBZ0JDLElBQXBCLEVBQTBCO0FBQ3RCLFFBQU1BLElBQUksR0FBR0MsU0FBUyxDQUFDSCxNQUFNLENBQUNDLFFBQVAsQ0FBZ0JDLElBQWpCLENBQXRCOztBQUNBLFFBQUlBLElBQUksQ0FBQyxjQUFELENBQUosSUFDQUEsSUFBSSxDQUFDLFlBQUQsQ0FEUixFQUN3QjtBQUNwQlIsa0JBQVksQ0FBQ1UsT0FBYixDQUFxQmIsNkJBQXJCLEVBQW9EVyxJQUFJLENBQUMsY0FBRCxDQUF4RDtBQUNBUixrQkFBWSxDQUFDVSxPQUFiLENBQXFCWixvQ0FBckIsRUFBMkRNLElBQUksQ0FBQ0MsR0FBTCxLQUFhLE1BQU1GLFFBQVEsQ0FBQ0ssSUFBSSxDQUFDLFlBQUQsQ0FBTCxDQUF0RjtBQUNBRixZQUFNLENBQUNDLFFBQVAsR0FBa0IsR0FBbEI7QUFDQUwsaUJBQVcsR0FBRyxJQUFkO0FBQ0g7QUFDSjtBQUNKOztBQUVELElBQUlTLFFBQVEsR0FBRyxFQUFmO0FBSUEsSUFBSUMsR0FBRyxHQUFHLElBQUlDLEtBQUosRUFBVjs7QUFHQXJCLE1BQU0sQ0FBQ3NCLFlBQVAsR0FBc0IsVUFBVUMsSUFBVixFQUFnQjtBQUNsQyxNQUFJQyxhQUFhLEdBQUVELElBQUksQ0FBQ0UsUUFBeEI7QUFDQUMsYUFBVztBQUNYQyxZQUFVLENBQUMsWUFBVTtBQUNqQixRQUFNQyxhQUFhLEdBQUdDLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixnQkFBeEIsQ0FBdEI7QUFDQUYsaUJBQWEsQ0FBQ0csS0FBZCxDQUFvQkMsS0FBcEIsR0FBNEJILFFBQVEsQ0FBQ0ksSUFBVCxDQUFjQyxXQUExQztBQUNBTixpQkFBYSxDQUFDSSxLQUFkLEdBQXNCSCxRQUFRLENBQUNJLElBQVQsQ0FBY0MsV0FBcEM7QUFFQSxRQUFNRixLQUFLLEdBQUdKLGFBQWEsQ0FBQ0ksS0FBNUI7QUFDQSxRQUFNRyxHQUFHLEdBQUdQLGFBQWEsQ0FBQ1EsVUFBZCxDQUF5QixJQUF6QixDQUFaLENBTmlCLENBUWpCOztBQUNBWixpQkFBYSxDQUFDYSxPQUFkLENBQXNCLFVBQUNDLE9BQUQsRUFBVUMsWUFBVixFQUEyQjtBQUM3Q0osU0FBRyxDQUFDSyxTQUFKLEdBQWdCdEMsTUFBTSxDQUFDcUMsWUFBWSxHQUFHckMsTUFBTSxDQUFDdUMsTUFBdkIsQ0FBdEI7QUFDQU4sU0FBRyxDQUFDTyxRQUFKLENBQWFKLE9BQU8sQ0FBQ0ssS0FBUixHQUFnQnBCLElBQUksQ0FBQ3FCLEtBQUwsQ0FBV0MsUUFBM0IsR0FBc0NiLEtBQW5ELEVBQTBELENBQTFELEVBQTZETSxPQUFPLENBQUNPLFFBQVIsR0FBbUJ0QixJQUFJLENBQUNxQixLQUFMLENBQVdDLFFBQTlCLEdBQXlDYixLQUF0RyxFQUE2R0osYUFBYSxDQUFDa0IsTUFBM0g7QUFDSCxLQUhELEVBVGlCLENBY2pCOztBQUNBLGFBQVNDLHFCQUFULENBQStCQyxTQUEvQixFQUEwQztBQUN0Qy9DLFlBQU0sSUFBSUEsTUFBTSxDQUFDZ0QsZUFBUCxHQUF5QkMsSUFBekIsQ0FBOEIsVUFBQUMsS0FBSyxFQUFJO0FBQzdDaEIsV0FBRyxDQUFDaUIsU0FBSixDQUFjLENBQWQsRUFBaUIsQ0FBakIsRUFBb0J4QixhQUFhLENBQUNJLEtBQWxDLEVBQXlDSixhQUFhLENBQUNrQixNQUF2RDtBQUNBWCxXQUFHLENBQUNrQixTQUFKLENBQWNqQyxHQUFkLEVBQW1CLENBQW5CLEVBQXNCLENBQXRCO0FBQ0FlLFdBQUcsQ0FBQ0ssU0FBSixHQUFnQixNQUFoQjtBQUNBLFlBQU1jLFFBQVEsR0FBR0gsS0FBSyxDQUFDRyxRQUFOLEdBQWlCLElBQWpCLEdBQXdCL0IsSUFBSSxDQUFDcUIsS0FBTCxDQUFXQyxRQUFuQyxHQUE4Q2IsS0FBL0Q7QUFDQUcsV0FBRyxDQUFDTyxRQUFKLENBQWFZLFFBQVEsR0FBRyxDQUF4QixFQUEyQixDQUEzQixFQUE4QixDQUE5QixFQUFpQzFCLGFBQWEsQ0FBQ2tCLE1BQS9DO0FBQ0FoQyxjQUFNLENBQUN5QyxxQkFBUCxDQUE2QlIscUJBQTdCO0FBQ0gsT0FQUyxFQU9QUyxLQVBPLENBT0QsVUFBQUMsQ0FBQyxFQUFJO0FBQ1ZDLGVBQU8sQ0FBQ0MsS0FBUixDQUFjLGFBQWQsRUFBNkJGLENBQTdCO0FBQ0EzQyxjQUFNLENBQUN5QyxxQkFBUCxDQUE2QlIscUJBQTdCO0FBQ0gsT0FWUyxDQUFWO0FBV0g7O0FBRURqQyxVQUFNLENBQUN5QyxxQkFBUCxDQUE2QlIscUJBQTdCO0FBQ0EzQixPQUFHLENBQUN3QyxHQUFKLEdBQVVoQyxhQUFhLENBQUNpQyxTQUFkLENBQXdCLEtBQXhCLENBQVY7QUFDSCxHQS9CUyxFQStCUixHQS9CUSxDQUFWO0FBZ0NILENBbkNELEMsQ0FzQ0E7OztBQUNBN0QsTUFBTSxDQUFDOEQsV0FBUCxHQUFxQixVQUFVQyxFQUFWLEVBQWM7QUFDL0J6QixTQUFPLEdBQUdULFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixVQUF4QixDQUFWO0FBQ0FRLFNBQU8sQ0FBQzBCLFVBQVIsQ0FBbUJDLE1BQW5CO0FBQ0EsTUFBSUMsS0FBSyxHQUFHLGtCQUFrQkgsRUFBOUI7QUFDQWxDLFVBQVEsQ0FBQ0MsY0FBVCxDQUF3QixXQUF4QixFQUFxQ3FDLFlBQXJDLENBQWtELEtBQWxELEVBQXlELHVCQUF6RDtBQUNBQyxXQUFTLEdBQUcsSUFBWjtBQUNBdkMsVUFBUSxDQUFDQyxjQUFULENBQXdCLFNBQXhCLEVBQW1DQyxLQUFuQyxDQUF5Q3NDLE1BQXpDLEdBQWtELE1BQWxEO0FBQ0F4QyxVQUFRLENBQUNDLGNBQVQsQ0FBd0IsT0FBeEIsRUFBaUNDLEtBQWpDLENBQXVDdUMsT0FBdkMsR0FBaUQsTUFBakQ7QUFDQXpDLFVBQVEsQ0FBQ0MsY0FBVCxDQUF3QixTQUF4QixFQUFtQ3lDLFNBQW5DLENBQTZDQyxHQUE3QyxDQUFpRCxTQUFqRDtBQUNBM0MsVUFBUSxDQUFDQyxjQUFULENBQXdCLFlBQXhCLEVBQXNDeUMsU0FBdEMsQ0FBZ0RDLEdBQWhELENBQW9ELFFBQXBEO0FBQ0EzQyxVQUFRLENBQUNDLGNBQVQsQ0FBd0IsU0FBeEIsRUFBbUNDLEtBQW5DLENBQXlDdUMsT0FBekMsR0FBbUQsTUFBbkQ7QUFDQSxTQUFPRyxLQUFLLENBQUNQLEtBQUQsQ0FBTCxDQUFhaEIsSUFBYixDQUFrQixVQUFBTyxDQUFDO0FBQUEsV0FBSUEsQ0FBQyxDQUFDaUIsSUFBRixFQUFKO0FBQUEsR0FBbkIsRUFBaUN4QixJQUFqQyxDQUFzQyxVQUFBM0IsSUFBSSxFQUFJO0FBQ2pEcEIsYUFBUyxHQUFHb0IsSUFBWjtBQUNBRCxnQkFBWSxDQUFDQyxJQUFELENBQVo7QUFDQWtELFNBQUssb0RBQTZDdEQsUUFBUSx5QkFBa0JBLFFBQWxCLENBQXJELEdBQXFGO0FBQ3RGd0QsWUFBTSxFQUFFLEtBRDhFO0FBRXRGMUMsVUFBSSxFQUFFMkMsSUFBSSxDQUFDQyxTQUFMLENBQWU7QUFBQyxnQkFBUSx5QkFBa0JkLEVBQWxCO0FBQVQsT0FBZixDQUZnRjtBQUd0RmUsYUFBTyxFQUFFO0FBQ0wsMENBQTJCdkUsV0FBM0I7QUFESztBQUg2RSxLQUFyRixDQUFMLENBTUdpRCxLQU5ILENBTVMsVUFBQUMsQ0FBQztBQUFBLGFBQUlDLE9BQU8sQ0FBQ0MsS0FBUixDQUFjRixDQUFkLENBQUo7QUFBQSxLQU5WO0FBT0gsR0FWTSxDQUFQO0FBV0gsQ0F0QkQsQyxDQXlCQTs7O0FBQ0EsSUFBSVcsU0FBUyxHQUFHLElBQWhCOztBQUNBcEUsTUFBTSxDQUFDK0UsdUJBQVAsR0FBaUMsWUFBWTtBQUN6QzlFLFFBQU0sR0FBRyxJQUFJRCxNQUFNLENBQUNnRixPQUFQLENBQWVDLE1BQW5CLENBQTBCO0FBQy9CQyxRQUFJLEVBQUUsdUJBRHlCO0FBRS9CQyxpQkFBYSxFQUFFLHVCQUFVQyxRQUFWLEVBQW9CO0FBQy9CQSxjQUFRLENBQUM3RSxXQUFELENBQVI7QUFDSCxLQUo4QjtBQUsvQjhFLFVBQU0sRUFBRTtBQUx1QixHQUExQixDQUFULENBRHlDLENBU3pDOztBQUNBcEYsUUFBTSxDQUFDcUYsRUFBUCxDQUFVLE9BQVYsRUFBbUIsVUFBVS9ELElBQVYsRUFBZ0I7QUFDL0JKLFlBQVEsR0FBR0ksSUFBSSxDQUFDZ0UsU0FBaEI7QUFDQTVELGNBQVUsQ0FBQyxZQUFNO0FBQ2I4QyxXQUFLLENBQUMsc0NBQUQsRUFBeUM7QUFDMUNFLGNBQU0sRUFBRSxLQURrQztBQUUxQzFDLFlBQUksRUFBRTJDLElBQUksQ0FBQ0MsU0FBTCxDQUFlO0FBQ2pCVyxvQkFBVSxFQUFFLENBQ1JqRSxJQUFJLENBQUNnRSxTQURHLENBREs7QUFJakJFLGNBQUksRUFBRTtBQUpXLFNBQWYsQ0FGb0M7QUFRMUNYLGVBQU8sRUFBRTtBQUNMLDRDQUEyQnZFLFdBQTNCO0FBREs7QUFSaUMsT0FBekMsQ0FBTCxDQVdHaUQsS0FYSCxDQVdTLFVBQUFDLENBQUM7QUFBQSxlQUFJQyxPQUFPLENBQUNDLEtBQVIsQ0FBY0YsQ0FBZCxDQUFKO0FBQUEsT0FYVjtBQVlILEtBYlMsRUFhUCxHQWJPLENBQVY7QUFjSCxHQWhCRCxFQVZ5QyxDQTJCekM7O0FBQ0F4RCxRQUFNLENBQUN5RixPQUFQLEdBNUJ5QyxDQThCekM7O0FBQ0EsTUFBTUMsVUFBVSxHQUFHOUQsUUFBUSxDQUFDQyxjQUFULENBQXdCLFdBQXhCLENBQW5CO0FBRUE2RCxZQUFVLENBQUNDLGdCQUFYLENBQTRCLE9BQTVCLEVBQXFDLFVBQVVDLEtBQVYsRUFBaUI7QUFDbEQ1RixVQUFNLENBQUM2RixVQUFQOztBQUNBLFFBQUkxQixTQUFKLEVBQWU7QUFDWHVCLGdCQUFVLENBQUN4QixZQUFYLENBQXdCLEtBQXhCLEVBQStCLHNCQUEvQjtBQUNBQyxlQUFTLEdBQUcsS0FBWjtBQUNBdkMsY0FBUSxDQUFDQyxjQUFULENBQXdCLFNBQXhCLEVBQW1DQyxLQUFuQyxDQUF5Q3NDLE1BQXpDLEdBQWtELFdBQWxEO0FBQ0F4QyxjQUFRLENBQUNDLGNBQVQsQ0FBd0IsT0FBeEIsRUFBaUNDLEtBQWpDLENBQXVDdUMsT0FBdkMsR0FBaUQsT0FBakQ7QUFDQXpDLGNBQVEsQ0FBQ0MsY0FBVCxDQUF3QixTQUF4QixFQUFtQ3lDLFNBQW5DLENBQTZDQyxHQUE3QyxDQUFpRCxTQUFqRDtBQUNBM0MsY0FBUSxDQUFDQyxjQUFULENBQXdCLFNBQXhCLEVBQW1DQyxLQUFuQyxDQUF5Q3VDLE9BQXpDLEdBQW1ELE9BQW5EO0FBQ0gsS0FQRCxNQU9PO0FBQ0hxQixnQkFBVSxDQUFDeEIsWUFBWCxDQUF3QixLQUF4QixFQUErQix1QkFBL0I7QUFDQUMsZUFBUyxHQUFHLElBQVo7QUFDQXZDLGNBQVEsQ0FBQ0MsY0FBVCxDQUF3QixTQUF4QixFQUFtQ0MsS0FBbkMsQ0FBeUNzQyxNQUF6QyxHQUFrRCxNQUFsRDtBQUNBeEMsY0FBUSxDQUFDQyxjQUFULENBQXdCLE9BQXhCLEVBQWlDQyxLQUFqQyxDQUF1Q3VDLE9BQXZDLEdBQWlELE1BQWpEO0FBQ0F6QyxjQUFRLENBQUNDLGNBQVQsQ0FBd0IsU0FBeEIsRUFBbUN5QyxTQUFuQyxDQUE2Q0MsR0FBN0MsQ0FBaUQsU0FBakQ7QUFDQTNDLGNBQVEsQ0FBQ0MsY0FBVCxDQUF3QixTQUF4QixFQUFtQ0MsS0FBbkMsQ0FBeUN1QyxPQUF6QyxHQUFtRCxNQUFuRDtBQUNIO0FBQ0osR0FqQkQ7QUFrQkgsQ0FuREQ7O0FBcURBLFNBQVNyRCxTQUFULENBQW1CRCxJQUFuQixFQUF5QjtBQUNyQixTQUFPQSxJQUFJLENBQ04rRSxTQURFLENBQ1EsQ0FEUixFQUVGQyxLQUZFLENBRUksR0FGSixFQUdGQyxNQUhFLENBR0ssVUFBVUMsT0FBVixFQUFtQkMsSUFBbkIsRUFBeUI7QUFDN0IsUUFBSUEsSUFBSixFQUFVO0FBQ04sVUFBSUMsS0FBSyxHQUFHRCxJQUFJLENBQUNILEtBQUwsQ0FBVyxHQUFYLENBQVo7QUFDQUUsYUFBTyxDQUFDRSxLQUFLLENBQUMsQ0FBRCxDQUFOLENBQVAsR0FBb0JDLGtCQUFrQixDQUFDRCxLQUFLLENBQUMsQ0FBRCxDQUFOLENBQXRDO0FBQ0g7O0FBQ0QsV0FBT0YsT0FBUDtBQUNILEdBVEUsRUFTQSxFQVRBLENBQVA7QUFVSDs7QUFHRHJFLFFBQVEsQ0FBQytELGdCQUFULENBQTBCLGtCQUExQixFQUE4QyxZQUFNO0FBRWhEO0FBQ0EsTUFBSWxGLFdBQUosRUFBaUI7QUFDYm1CLFlBQVEsQ0FBQ0MsY0FBVCxDQUF3QixhQUF4QixFQUF1Q3dFLFNBQXZDLEdBQW1ELGlJQUFuRCxDQURhLENBR2I7O0FBQ0EsUUFBTUMsS0FBSyxHQUFHMUUsUUFBUSxDQUFDMkUsYUFBVCxDQUF1QixPQUF2QixDQUFkO0FBQ0FELFNBQUssQ0FBQ1gsZ0JBQU4sQ0FBdUIsT0FBdkIsRUFBZ0MsVUFBVUMsS0FBVixFQUFpQjtBQUM3QyxVQUFJVSxLQUFLLENBQUNFLEtBQU4sQ0FBWWhFLE1BQVosSUFBc0IsQ0FBMUIsRUFBNkI7QUFDekIsWUFBTWlFLFdBQVcsR0FBRyxtQkFBb0IsVUFBQXhDLEtBQUs7QUFBQSxpQkFBSSxDQUFDQSxLQUFELEdBQVMsb0JBQVQsR0FBZ0NBLEtBQXBDO0FBQUEsU0FBTixDQUFpRHFDLEtBQUssQ0FBQ0UsS0FBdkQsQ0FBdkM7O0FBQ0FoQyxhQUFLLENBQUNpQyxXQUFELENBQUwsQ0FBbUJ4RCxJQUFuQixDQUF3QixVQUFBTyxDQUFDO0FBQUEsaUJBQUlBLENBQUMsQ0FBQ2lCLElBQUYsRUFBSjtBQUFBLFNBQXpCLEVBQXVDeEIsSUFBdkMsQ0FBNEMsVUFBQTNCLElBQUksRUFBSTtBQUNoRE0sa0JBQVEsQ0FBQ0MsY0FBVCxDQUF3QixTQUF4QixFQUFtQ3dFLFNBQW5DLEdBQStDL0UsSUFBSSxDQUFDb0YsTUFBTCxDQUFZQyxLQUFaLENBQzFDQyxHQUQwQyxDQUN0QyxVQUFBakUsS0FBSztBQUFBLG1GQUEwREEsS0FBSyxDQUFDbUIsRUFBaEUsaURBQXNHbkIsS0FBSyxDQUFDc0MsSUFBNUcsZ0RBQW9KdEMsS0FBSyxDQUFDa0UsT0FBTixDQUFjLENBQWQsRUFBaUI1QixJQUFySyw0RUFBc090QyxLQUFLLENBQUNtRSxLQUFOLENBQVlDLE1BQVosQ0FBbUIsQ0FBbkIsRUFBc0JDLEdBQTVQO0FBQUEsV0FEaUMsRUFFMUNDLElBRjBDLENBRXJDLElBRnFDLENBQS9DO0FBR0gsU0FKRCxFQUlHMUQsS0FKSCxDQUlTLFVBQUFHLEtBQUssRUFBSTtBQUNkOUIsa0JBQVEsQ0FBQ0MsY0FBVCxDQUF3QixTQUF4QixFQUFtQ3dFLFNBQW5DLEdBQStDM0MsS0FBL0M7QUFDSCxTQU5EO0FBT0g7QUFDSixLQVhEO0FBYUgsR0FsQkQsTUFrQk87QUFFSDtBQUNBOUIsWUFBUSxDQUFDQyxjQUFULENBQXdCLGFBQXhCLEVBQXVDd0UsU0FBdkMsR0FBbUQsOEVBQW5EO0FBQ0F6RSxZQUFRLENBQUNDLGNBQVQsQ0FBd0IsT0FBeEIsRUFBaUM4RCxnQkFBakMsQ0FBa0QsT0FBbEQsRUFBMkQsVUFBVW5DLENBQVYsRUFBYTtBQUNwRUEsT0FBQyxDQUFDMEQsY0FBRjtBQUNBMUMsV0FBSyxDQUFDLHFCQUFELENBQUwsQ0FDS3ZCLElBREwsQ0FDVSxVQUFBTyxDQUFDO0FBQUEsZUFBSUEsQ0FBQyxDQUFDaUIsSUFBRixFQUFKO0FBQUEsT0FEWCxFQUVLeEIsSUFGTCxDQUVVLFVBQUEzQixJQUFJLEVBQUk7QUFDVlQsY0FBTSxDQUFDQyxRQUFQLEdBQWtCUSxJQUFJLENBQUM2RixXQUF2QjtBQUNILE9BSkwsRUFLSzVELEtBTEwsQ0FLVyxVQUFBRyxLQUFLLEVBQUk7QUFDWjBELGFBQUssQ0FBQyw4Q0FBRCxDQUFMO0FBQ0gsT0FQTDtBQVFILEtBVkQ7QUFXSDtBQUdKLENBdkNELEU7Ozs7Ozs7Ozs7OztBQ3JLQTtBQUNBQyxtQkFBTyxDQUFDLDJDQUFELENBQVAsQyxDQUVBOztBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDSkE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSw0Q0FBNEM7O0FBRTVDIiwiZmlsZSI6ImFwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbImdsb2JhbC5wbGF5ZXI7XG5nbG9iYWwuY29sb3JzID0gW107XG5nbG9iYWwuZGF0YVRyYWNrID0gbnVsbDtcbnZhciBlbGVtcyA9IFtdO1xuY29uc3QgTE9DQUxTVE9SQUdFX0FDQ0VTU19UT0tFTl9LRVkgPSAnc3BvdGlmeS1hdWRpby1hbmFseXNpcy1wbGF5YmFjay10b2tlbic7XG5jb25zdCBMT0NBTFNUT1JBR0VfQUNDRVNTX1RPS0VOX0VYUElSWV9LRVkgPSAnc3BvdGlmeS1hdWRpby1hbmFseXNpcy1wbGF5YmFjay10b2tlbi1leHBpcmVzLWluJztcbmNvbnN0IGFjY2Vzc1Rva2VuID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oTE9DQUxTVE9SQUdFX0FDQ0VTU19UT0tFTl9LRVkpO1xudmFyIGlzQ29ubmVjdGVkID0gZmFsc2U7XG5cbmlmIChsb2NhbFN0b3JhZ2UuZ2V0SXRlbShMT0NBTFNUT1JBR0VfQUNDRVNTX1RPS0VOX0tFWSkgJiZcbiAgICBwYXJzZUludChwYXJzZUludChsb2NhbFN0b3JhZ2UuZ2V0SXRlbShMT0NBTFNUT1JBR0VfQUNDRVNTX1RPS0VOX0VYUElSWV9LRVkpKSkgPiBEYXRlLm5vdygpKSB7XG4gICAgaXNDb25uZWN0ZWQgPSB0cnVlO1xufSBlbHNlIHtcbiAgICBpZiAod2luZG93LmxvY2F0aW9uLmhhc2gpIHtcbiAgICAgICAgY29uc3QgaGFzaCA9IHBhcnNlSGFzaCh3aW5kb3cubG9jYXRpb24uaGFzaCk7XG4gICAgICAgIGlmIChoYXNoWydhY2Nlc3NfdG9rZW4nXSAmJlxuICAgICAgICAgICAgaGFzaFsnZXhwaXJlc19pbiddKSB7XG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShMT0NBTFNUT1JBR0VfQUNDRVNTX1RPS0VOX0tFWSwgaGFzaFsnYWNjZXNzX3Rva2VuJ10pO1xuICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oTE9DQUxTVE9SQUdFX0FDQ0VTU19UT0tFTl9FWFBJUllfS0VZLCBEYXRlLm5vdygpICsgOTkwICogcGFyc2VJbnQoaGFzaFsnZXhwaXJlc19pbiddKSk7XG4gICAgICAgICAgICB3aW5kb3cubG9jYXRpb24gPSBcIi9cIjtcbiAgICAgICAgICAgIGlzQ29ubmVjdGVkID0gdHJ1ZTtcbiAgICAgICAgfVxuICAgIH1cbn1cblxubGV0IGRldmljZUlkID0gJyc7XG5cblxuXG52YXIgaW1nID0gbmV3IEltYWdlO1xuXG5cbmdsb2JhbC5kcmF3QW5hbHlzaXMgPSBmdW5jdGlvbiAoZGF0YSkge1xuICAgIHZhciBzZWN0aW9uc1RyYWNrID1kYXRhLnNlY3Rpb25zXG4gICAgc3RhcnRTa2V0Y2goKVxuICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcbiAgICAgICAgY29uc3QgZmVhdHVyZXNDaGFydCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdmZWF0dXJlcy1jaGFydCcpO1xuICAgICAgICBmZWF0dXJlc0NoYXJ0LnN0eWxlLndpZHRoID0gZG9jdW1lbnQuYm9keS5vZmZzZXRXaWR0aDtcbiAgICAgICAgZmVhdHVyZXNDaGFydC53aWR0aCA9IGRvY3VtZW50LmJvZHkub2Zmc2V0V2lkdGg7XG5cbiAgICAgICAgY29uc3Qgd2lkdGggPSBmZWF0dXJlc0NoYXJ0LndpZHRoO1xuICAgICAgICBjb25zdCBjdHggPSBmZWF0dXJlc0NoYXJ0LmdldENvbnRleHQoXCIyZFwiKTtcblxuICAgICAgICAvL0Nyw6llciBsZXMgc2VjdGlvbnNcbiAgICAgICAgc2VjdGlvbnNUcmFjay5mb3JFYWNoKChzZWN0aW9uLCBzZWN0aW9uSW5kZXgpID0+IHtcbiAgICAgICAgICAgIGN0eC5maWxsU3R5bGUgPSBjb2xvcnNbc2VjdGlvbkluZGV4ICUgY29sb3JzLmxlbmd0aF07XG4gICAgICAgICAgICBjdHguZmlsbFJlY3Qoc2VjdGlvbi5zdGFydCAvIGRhdGEudHJhY2suZHVyYXRpb24gKiB3aWR0aCwgMCwgc2VjdGlvbi5kdXJhdGlvbiAvIGRhdGEudHJhY2suZHVyYXRpb24gKiB3aWR0aCwgZmVhdHVyZXNDaGFydC5oZWlnaHQpO1xuICAgICAgICB9KTtcblxuICAgICAgICAvL0JhcnJlIGRlIGTDqWZpbGVtZW50IGRlIGxhIG11c2lxdWVcbiAgICAgICAgZnVuY3Rpb24gcHJvdmlkZUFuaW1hdGlvbkZyYW1lKHRpbWVzdGFtcCkge1xuICAgICAgICAgICAgcGxheWVyICYmIHBsYXllci5nZXRDdXJyZW50U3RhdGUoKS50aGVuKHN0YXRlID0+IHtcbiAgICAgICAgICAgICAgICBjdHguY2xlYXJSZWN0KDAsIDAsIGZlYXR1cmVzQ2hhcnQud2lkdGgsIGZlYXR1cmVzQ2hhcnQuaGVpZ2h0KTtcbiAgICAgICAgICAgICAgICBjdHguZHJhd0ltYWdlKGltZywgMCwgMCk7XG4gICAgICAgICAgICAgICAgY3R4LmZpbGxTdHlsZSA9IFwiIzAwMFwiO1xuICAgICAgICAgICAgICAgIGNvbnN0IHBvc2l0aW9uID0gc3RhdGUucG9zaXRpb24gLyAxMDAwIC8gZGF0YS50cmFjay5kdXJhdGlvbiAqIHdpZHRoXG4gICAgICAgICAgICAgICAgY3R4LmZpbGxSZWN0KHBvc2l0aW9uIC0gMiwgMCwgNSwgZmVhdHVyZXNDaGFydC5oZWlnaHQpO1xuICAgICAgICAgICAgICAgIHdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUocHJvdmlkZUFuaW1hdGlvbkZyYW1lKTtcbiAgICAgICAgICAgIH0pLmNhdGNoKGUgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJBbmltYXRpb246IFwiLCBlKTtcbiAgICAgICAgICAgICAgICB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lKHByb3ZpZGVBbmltYXRpb25GcmFtZSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIHdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUocHJvdmlkZUFuaW1hdGlvbkZyYW1lKTtcbiAgICAgICAgaW1nLnNyYyA9IGZlYXR1cmVzQ2hhcnQudG9EYXRhVVJMKCdwbmcnKTtcbiAgICB9LDEwMClcbn1cblxuXG4vL0F2b2lyIGwnYW5hbHlzZSBkdSBzb25cbmdsb2JhbC5nZXRBbmFseXNpcyA9IGZ1bmN0aW9uIChpZCkge1xuICAgIHNlY3Rpb24gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInNlY3Rpb24xXCIpO1xuICAgIHNlY3Rpb24uZmlyc3RDaGlsZC5yZW1vdmUoKTtcbiAgICBsZXQgcXVlcnkgPSAnL2FuYWx5c2lzP2lkPScgKyBpZDtcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnaW1nQnV0dG9uJykuc2V0QXR0cmlidXRlKCdzcmMnLCAnL2ltZy9wYXVzZS1zeW1ib2wucG5nJyk7XG4gICAgaXNQbGF5aW5nID0gdHJ1ZTtcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImFuYWx5c2VcIikuc3R5bGUuZmlsdGVyID0gXCJub25lXCI7XG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJwb3B1cFwiKS5zdHlsZS5kaXNwbGF5ID0gXCJub25lXCI7XG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJiZ19sb2dvXCIpLmNsYXNzTGlzdC5hZGQoXCJmYWRlb3V0XCIpO1xuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicGxheUJ1dHRvblwiKS5jbGFzc0xpc3QuYWRkKFwiZmFkZWluXCIpO1xuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYmdfYmx1clwiKS5zdHlsZS5kaXNwbGF5ID0gXCJub25lXCI7XG4gICAgcmV0dXJuIGZldGNoKHF1ZXJ5KS50aGVuKGUgPT4gZS5qc29uKCkpLnRoZW4oZGF0YSA9PiB7XG4gICAgICAgIGRhdGFUcmFjayA9IGRhdGE7XG4gICAgICAgIGRyYXdBbmFseXNpcyhkYXRhKTtcbiAgICAgICAgZmV0Y2goYGh0dHBzOi8vYXBpLnNwb3RpZnkuY29tL3YxL21lL3BsYXllci9wbGF5JHtkZXZpY2VJZCAmJiBgP2RldmljZV9pZD0ke2RldmljZUlkfWB9YCwge1xuICAgICAgICAgICAgbWV0aG9kOiBcIlBVVFwiLFxuICAgICAgICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoe1widXJpc1wiOiBbYHNwb3RpZnk6dHJhY2s6JHtpZH1gXX0pLFxuICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICdBdXRob3JpemF0aW9uJzogYEJlYXJlciAke2FjY2Vzc1Rva2VufWBcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSkuY2F0Y2goZSA9PiBjb25zb2xlLmVycm9yKGUpKTtcbiAgICB9KTtcbn07XG5cblxuLy9MaXJlIGxhIGNoYW5zb24gY2hvaXNpXG52YXIgaXNQbGF5aW5nID0gdHJ1ZTtcbmdsb2JhbC5vblNwb3RpZnlQbGF5ZXJBUElSZWFkeSA9IGZ1bmN0aW9uICgpIHtcbiAgICBwbGF5ZXIgPSBuZXcgZ2xvYmFsLlNwb3RpZnkuUGxheWVyKHtcbiAgICAgICAgbmFtZTogJ0F1ZGlvIEFuYWx5c2lzIFBsYXllcicsXG4gICAgICAgIGdldE9hdXRoVG9rZW46IGZ1bmN0aW9uIChjYWxsYmFjaykge1xuICAgICAgICAgICAgY2FsbGJhY2soYWNjZXNzVG9rZW4pO1xuICAgICAgICB9LFxuICAgICAgICB2b2x1bWU6IDAuOFxuICAgIH0pO1xuXG4gICAgLy8gUmVhZHlcbiAgICBwbGF5ZXIub24oJ3JlYWR5JywgZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgZGV2aWNlSWQgPSBkYXRhLmRldmljZV9pZDtcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICBmZXRjaCgnaHR0cHM6Ly9hcGkuc3BvdGlmeS5jb20vdjEvbWUvcGxheWVyJywge1xuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJQVVRcIixcbiAgICAgICAgICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeSh7XG4gICAgICAgICAgICAgICAgICAgIGRldmljZV9pZHM6IFtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGEuZGV2aWNlX2lkXG4gICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgIHBsYXk6IGZhbHNlXG4gICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICAnQXV0aG9yaXphdGlvbic6IGBCZWFyZXIgJHthY2Nlc3NUb2tlbn1gXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSkuY2F0Y2goZSA9PiBjb25zb2xlLmVycm9yKGUpKTtcbiAgICAgICAgfSwgMTAwKTtcbiAgICB9KTtcbiAgICAvLyBDb25uZWN0IHRvIHRoZSBwbGF5ZXIhXG4gICAgcGxheWVyLmNvbm5lY3QoKTtcblxuICAgIC8vUGxheSBvdSBwYXVzZSB0aGUgcGxheWVyXG4gICAgY29uc3QgcGxheUJ1dHRvbiA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdpbWdCdXR0b24nKTtcblxuICAgIHBsYXlCdXR0b24uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgcGxheWVyLnRvZ2dsZVBsYXkoKTtcbiAgICAgICAgaWYgKGlzUGxheWluZykge1xuICAgICAgICAgICAgcGxheUJ1dHRvbi5zZXRBdHRyaWJ1dGUoJ3NyYycsICcvaW1nL3BsYXktYnV0dG9uLnBuZycpO1xuICAgICAgICAgICAgaXNQbGF5aW5nID0gZmFsc2U7XG4gICAgICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImFuYWx5c2VcIikuc3R5bGUuZmlsdGVyID0gXCJibHVyKDNweClcIjtcbiAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicG9wdXBcIikuc3R5bGUuZGlzcGxheSA9IFwiYmxvY2tcIjtcbiAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYmdfbG9nb1wiKS5jbGFzc0xpc3QuYWRkKFwiZmFkZW91dFwiKTtcbiAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYmdfYmx1clwiKS5zdHlsZS5kaXNwbGF5ID0gXCJibG9ja1wiO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcGxheUJ1dHRvbi5zZXRBdHRyaWJ1dGUoJ3NyYycsICcvaW1nL3BhdXNlLXN5bWJvbC5wbmcnKVxuICAgICAgICAgICAgaXNQbGF5aW5nID0gdHJ1ZTtcbiAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYW5hbHlzZVwiKS5zdHlsZS5maWx0ZXIgPSBcIm5vbmVcIjtcbiAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicG9wdXBcIikuc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xuICAgICAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJiZ19sb2dvXCIpLmNsYXNzTGlzdC5hZGQoXCJmYWRlb3V0XCIpO1xuICAgICAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJiZ19ibHVyXCIpLnN0eWxlLmRpc3BsYXkgPSBcIm5vbmVcIjtcbiAgICAgICAgfVxuICAgIH0pO1xufVxuXG5mdW5jdGlvbiBwYXJzZUhhc2goaGFzaCkge1xuICAgIHJldHVybiBoYXNoXG4gICAgICAgIC5zdWJzdHJpbmcoMSlcbiAgICAgICAgLnNwbGl0KCcmJylcbiAgICAgICAgLnJlZHVjZShmdW5jdGlvbiAoaW5pdGlhbCwgaXRlbSkge1xuICAgICAgICAgICAgaWYgKGl0ZW0pIHtcbiAgICAgICAgICAgICAgICB2YXIgcGFydHMgPSBpdGVtLnNwbGl0KCc9Jyk7XG4gICAgICAgICAgICAgICAgaW5pdGlhbFtwYXJ0c1swXV0gPSBkZWNvZGVVUklDb21wb25lbnQocGFydHNbMV0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGluaXRpYWw7XG4gICAgICAgIH0sIHt9KTtcbn1cblxuXG5kb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgKCkgPT4ge1xuXG4gICAgLy9VdGlsaXNhdGV1ciBBdXRoXG4gICAgaWYgKGlzQ29ubmVjdGVkKSB7XG4gICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdpc0Nvbm5lY3RlZCcpLmlubmVySFRNTCA9ICc8aW5wdXQgaWQ9XCJzZWFyY2hcIiB0eXBlPVwidGV4dFwiIG1heGxlbmd0aD1cIjEwMFwiIHBsYWNlaG9sZGVyPVwiQ2hvaXNpciB1bmUgbXVzaXF1ZVwiPjxpbWcgc3JjPVwiL2ltZy9sb29wLXNwb3RpZnkucG5nXCIgYWx0PVwiZmR2ZlwiIC8+JztcblxuICAgICAgICAvL0NoZXJjaGVyIHVuZSBtdXNpcXVlXG4gICAgICAgIGNvbnN0IGlucHV0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignaW5wdXQnKTtcbiAgICAgICAgaW5wdXQuYWRkRXZlbnRMaXN0ZW5lcignaW5wdXQnLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgIGlmIChpbnB1dC52YWx1ZS5sZW5ndGggPj0gMykge1xuICAgICAgICAgICAgICAgIGNvbnN0IHNlYXJjaFF1ZXJ5ID0gJy9zZWFyY2g/cXVlcnk9JyArIChxdWVyeSA9PiAhcXVlcnkgPyBcImN1dCB0byB0aGUgZmVlbGluZ1wiIDogcXVlcnkpKGlucHV0LnZhbHVlKTtcbiAgICAgICAgICAgICAgICBmZXRjaChzZWFyY2hRdWVyeSkudGhlbihlID0+IGUuanNvbigpKS50aGVuKGRhdGEgPT4ge1xuICAgICAgICAgICAgICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgncmVzdWx0cycpLmlubmVySFRNTCA9IGRhdGEudHJhY2tzLml0ZW1zXG4gICAgICAgICAgICAgICAgICAgICAgICAubWFwKHRyYWNrID0+IGA8bGkgY2xhc3M9XCJ0ZXh0LXNhbG1vblwiIG9uQ2xpY2s9XCJnZXRBbmFseXNpcygmYXBvczske3RyYWNrLmlkfSZhcG9zOylcIj48c3BhbiBjbGFzcz1cInRyYWNrTmFtZVwiPiR7dHJhY2submFtZX08L3NwYW4+PHNwYW4gY2xhc3M9XCJ0cmFja0FydGlzdFwiPiR7dHJhY2suYXJ0aXN0c1swXS5uYW1lfTwvc3Bhbj48c3BhbiBjbGFzcz1cInRyYWNrSW1nXCI+PGltZyBjbGFzcz1cImltZ0FsYnVsbVwiIHNyYz1cIiR7dHJhY2suYWxidW0uaW1hZ2VzWzJdLnVybH1cIj48L3NwYW4+PC9saT5gKVxuICAgICAgICAgICAgICAgICAgICAgICAgLmpvaW4oJ1xcbicpO1xuICAgICAgICAgICAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgICAgICAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3Jlc3VsdHMnKS5pbm5lckhUTUwgPSBlcnJvcjtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICB9IGVsc2Uge1xuXG4gICAgICAgIC8vVXRpbGlzYXRldXIgTm9uLSBBdXRoXG4gICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdpc0Nvbm5lY3RlZCcpLmlubmVySFRNTCA9ICc8YSBpZD1cImxvZ2luXCIgaHJlZj1cIlwiIGNsYXNzPVwiYnRuIGJ0bi1zbSBidG4tcHJpbWFyeVwiPkxvZyBJbiBXaXRoIFNwb3RpZnk8L2E+JztcbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2xvZ2luJykuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgZmV0Y2goJy9zcG90aWZ5UmVkaXJlY3RVcmknKVxuICAgICAgICAgICAgICAgIC50aGVuKGUgPT4gZS5qc29uKCkpXG4gICAgICAgICAgICAgICAgLnRoZW4oZGF0YSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbiA9IGRhdGEucmVkaXJlY3RVcmk7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICAgICAgICAgICAgICBhbGVydChcIkZhaWxlZCB0byBwcmVwYXJlIGZvciBTcG90aWZ5IEF1dGhlbnRpY2F0aW9uXCIpXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuXG59KTtcbiIsIi8vIEltcG9ydCBvbGQgYW5hbHlzaXMuanNcbnJlcXVpcmUoJy4vYW5hbHlzaXMnKTtcblxuLy8vLy8vLy8vLy8vLy8vLy8vL1xuLypcbmltcG9ydCBwNSBmcm9tICdwNSc7XG5cbi8hKipcbiAqIERFRklORSBBTEwgR0xBQkFMIFZBUklBQkxFUyBIRVJFXG4gKiEvXG5sZXQgd2lkdGggPSB3aW5kb3cuaW5uZXJXaWR0aDtcbmxldCBoZWlnaHQgPSB3aW5kb3cuaW5uZXJIZWlnaHQgLSAxMDA7XG5cbmxldCBteUNhbnZhcztcbmxldCBjMTtcbmxldCBjMjtcbmxldCBjMztcbmxldCBjNDtcbmxldCByYWRpdXNDb2VmID0gMDtcbmxldCBtYXhTY2FsZSA9IChoZWlnaHQgLyA1MCk7XG5sZXQgc2VjdGlvbnMgPSBbXTtcbmxldCB0cmFja1Byb2dyZXNzID0gMDtcblxuY29uc3Qgc2tldGNoID0gKHA1KSA9PiB7XG4gICAgcDUucHJlbG9hZCA9ICgpID0+IHtcbiAgICB9O1xuXG4gICAgcDUuc2V0dXAgPSAoKSA9PiB7XG4gICAgICAgIGMxID0gcDUuY29sb3IoXCIjNjg3NkFDXCIpO1xuICAgICAgICBjMiA9IHA1LmNvbG9yKFwiI0Q4MzE1QlwiKTtcbiAgICAgICAgYzMgPSBwNS5jb2xvcihcIiNEODMxNUJcIik7XG4gICAgICAgIGM0ID0gcDUuY29sb3IoXCIjRTU3OTk1XCIpO1xuICAgICAgICBteUNhbnZhcyA9IHA1LmNyZWF0ZUNhbnZhcyh3aWR0aCwgaGVpZ2h0KTtcbiAgICAgICAgbXlDYW52YXMucGFyZW50KCdzZWN0aW9uMScpO1xuICAgICAgICBwNS5ub1N0cm9rZSgpO1xuICAgIH07XG5cbiAgICBwNS5kcmF3ID0gKCkgPT4ge1xuICAgICAgICAvLyBUSEUgRFJBQUFBV1dXV1dXXG5cbiAgICAgICAgcGxheWVyLmdldEN1cnJlbnRTdGF0ZSgpLnRoZW4oc3RhdGUgPT4ge1xuICAgICAgICAgICAgaWYgKGRhdGFUcmFjayAhPSBudWxsICYmIHN0YXRlLnBhdXNlZCA9PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgIHRyYWNrUHJvZ3Jlc3MgPSAoc3RhdGUucG9zaXRpb24gLyBzdGF0ZS5kdXJhdGlvbikgKiAxMDA7XG4gICAgICAgICAgICAgICAgaWYgKHN0YXRlLnBvc2l0aW9uICsgMTAwMCA+PSAoZGF0YVRyYWNrLnNlY3Rpb25zWzBdLnN0YXJ0ICogMTAwMCkpIHtcbiAgICAgICAgICAgICAgICAgICAgZGF0YVRyYWNrLnNlY3Rpb25zWzBdLmluaXRQcm9ncmVzcyA9ICh0cmFja1Byb2dyZXNzKTtcbiAgICAgICAgICAgICAgICAgICAgc2VjdGlvbnMucHVzaChkYXRhVHJhY2suc2VjdGlvbnNbMF0pO1xuXG4gICAgICAgICAgICAgICAgICAgIGRhdGFUcmFjay5zZWN0aW9ucy5zaGlmdCgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIHA1LmJhY2tncm91bmQoMjU1KTtcbiAgICAgICAgc2VjdGlvbnMuZm9yRWFjaCgoc2VjdGlvbiwgc2VjdGlvbkluZGV4KSA9PiB7XG4gICAgICAgICAgICB2YXIgc2NhbGVQcm9ncmVzcyA9IG1heFNjYWxlIC8gKDEwMCAvICh0cmFja1Byb2dyZXNzIC0gc2VjdGlvbi5pbml0UHJvZ3Jlc3MpKTtcbiAgICAgICAgICAgIHA1LnB1c2goKTtcbiAgICAgICAgICAgIHA1LnRyYW5zbGF0ZSh3aWR0aCAqIDAuNSwgaGVpZ2h0ICogMC41KTtcbiAgICAgICAgICAgIGlmIChzZWN0aW9uSW5kZXggJSAyID09IDApIHtcbiAgICAgICAgICAgICAgICBwNS5yb3RhdGUoc2NhbGVQcm9ncmVzcyAqIChzZWN0aW9uSW5kZXggKyAxKSk7XG4gICAgICAgICAgICAgICAgcDUuZmlsbChjMSlcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcDUucm90YXRlKC1zY2FsZVByb2dyZXNzICogKHNlY3Rpb25JbmRleCArIDEpKTtcbiAgICAgICAgICAgICAgICBwNS5maWxsKGMyKVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBwNS5zY2FsZShzY2FsZVByb2dyZXNzKTtcbiAgICAgICAgICAgIHN0YXIoMCwgMCwgMjUsIDUwLCBwNS5yb3VuZChzZWN0aW9uLmR1cmF0aW9uKSk7XG5cbiAgICAgICAgICAgIHA1LnBvcCgpO1xuICAgICAgICB9KTtcbiAgICB9O1xufTtcblxubGV0IGNhbnZhcyA9IG5ldyBwNShza2V0Y2gpO1xuXG5mdW5jdGlvbiBzdGFyKHgsIHksIHJhZGl1czEsIHJhZGl1czIsIG5wb2ludHMpIHtcbiAgICB2YXIgYW5nbGUgPSBwNS5UV09fUEkgLyBucG9pbnRzO1xuICAgIHZhciBoYWxmQW5nbGUgPSBhbmdsZSAvIDIuMDtcbiAgICBwNS5iZWdpblNoYXBlKCk7XG4gICAgZm9yICh2YXIgYSA9IDA7IGEgPCBwNS5UV09fUEk7IGEgKz0gYW5nbGUpIHtcbiAgICAgICAgdmFyIHN4ID0geCArIHA1LmNvcyhhKSAqIHJhZGl1czI7XG4gICAgICAgIHZhciBzeSA9IHkgKyBwNS5zaW4oYSkgKiByYWRpdXMyO1xuICAgICAgICB2ZXJ0ZXgoc3gsIHN5KTtcbiAgICAgICAgc3ggPSB4ICsgcDUuY29zKGEgKyBoYWxmQW5nbGUpICogcmFkaXVzMTtcbiAgICAgICAgc3kgPSB5ICsgcDUuc2luKGEgKyBoYWxmQW5nbGUpICogcmFkaXVzMTtcbiAgICAgICAgdmVydGV4KHN4LCBzeSk7XG4gICAgfVxuICAgIHA1LmVuZFNoYXBlKHA1LkNMT1NFKTtcbn0qL1xuIiwidmFyIGc7XG5cbi8vIFRoaXMgd29ya3MgaW4gbm9uLXN0cmljdCBtb2RlXG5nID0gKGZ1bmN0aW9uKCkge1xuXHRyZXR1cm4gdGhpcztcbn0pKCk7XG5cbnRyeSB7XG5cdC8vIFRoaXMgd29ya3MgaWYgZXZhbCBpcyBhbGxvd2VkIChzZWUgQ1NQKVxuXHRnID0gZyB8fCBuZXcgRnVuY3Rpb24oXCJyZXR1cm4gdGhpc1wiKSgpO1xufSBjYXRjaCAoZSkge1xuXHQvLyBUaGlzIHdvcmtzIGlmIHRoZSB3aW5kb3cgcmVmZXJlbmNlIGlzIGF2YWlsYWJsZVxuXHRpZiAodHlwZW9mIHdpbmRvdyA9PT0gXCJvYmplY3RcIikgZyA9IHdpbmRvdztcbn1cblxuLy8gZyBjYW4gc3RpbGwgYmUgdW5kZWZpbmVkLCBidXQgbm90aGluZyB0byBkbyBhYm91dCBpdC4uLlxuLy8gV2UgcmV0dXJuIHVuZGVmaW5lZCwgaW5zdGVhZCBvZiBub3RoaW5nIGhlcmUsIHNvIGl0J3Ncbi8vIGVhc2llciB0byBoYW5kbGUgdGhpcyBjYXNlLiBpZighZ2xvYmFsKSB7IC4uLn1cblxubW9kdWxlLmV4cG9ydHMgPSBnO1xuIl0sInNvdXJjZVJvb3QiOiIifQ==